import type {
    Response,
    SendDocumentRequest,
    ReceiptResource,
    AttachmentResource,
    SendAttachmentRequest,
    Document,
} from './types';
import getToken from '../Authenticate/getToken';
import axios from 'axios';
import FormData from 'form-data';
import { fileToBuffer } from './utils/fileToBuffer';
import { validateDocuments } from './utils/validateDocuments';
import protocolSEQ from './db/protocol';
import { Readable } from 'stream';
import mime from 'mime';

const sendDocument = async (
    token: string,
    document: Buffer,
    documentMetaData: SendDocumentRequest | SendAttachmentRequest,
    isAttachment: boolean = false,
    DocumentProtocolNo: string = '',
    config?: {
        apiBase?: string;
    },    
): Promise<ReceiptResource | AttachmentResource> => {
    const apiBase = config?.apiBase ?? process.env.KSHDE_API_BASE;
    if (!apiBase) throw new Error('KSHDE_API_BASE is not defined');
    const form = new FormData();
    const stream = new Readable({
        read() {
            this.push(document);
            this.push(null);
        },
    });
    //form.append('DocumentContent', fs.createReadStream('/tmp/test.pdf'), { knownLength: fs.statSync('/tmp/test.pdf').size });
    form.append('DocumentContent', stream, {
        knownLength: document.length,
        filename: documentMetaData.FileName,
        contentType: '' + mime.getType(documentMetaData.FileName),
    });
    form.append('DocumentMetadata', JSON.stringify(documentMetaData));
    const headers = form.getHeaders();
    const apiURL = isAttachment
        ? apiBase + `/documents/${DocumentProtocolNo}/attachments`
        : apiBase + '/documents';
    try {
        const response = await axios.post(apiURL, form, {
            headers: {
                ...headers,
                'Content-Length': form.getLengthSync(), //AXIOS BUG does not send correct content length!
                Authorization: `Bearer ${token}`,
            },
        });
        return response.data;
    } catch (error: any) {
        //if is error 400,403,404,500 then return the error
        if (error.response) {
            if (
                error.response.status === 400 ||
                error.response.status === 403 ||
                error.response.status === 404 ||
                error.response.status === 500
            ) {
                return error.response.data;
            }
        }
        console.log(error);
        return {} as unknown as ReceiptResource;
    }
};

export const posToShde = async (
    documents: Document[],
    recipientCode: number | number[],
    orgChartVersion: number = 1,
    config?: {
        apiBase?: string;
        sectorCode?: string;
        clientId?: string;
        clientSecret?: string;
    },
): Promise<Response[]> => {
    //validate documents
    validateDocuments(documents); //throws error if validation fails
    //get token
    const token = await getToken(config);
    //prepare SendDocumentRequest for the first document
    const document = documents[0];
    const recipientCodes = Array.isArray(recipientCode)
        ? recipientCode
        : [recipientCode];
    const FirstDocumentRequest: SendDocumentRequest = {
        FileName:
            '' +
            (document.FileName ||
                (typeof document.FileDataOrPath === 'string'
                    ? (document.FileDataOrPath as string).split('/').pop()
                    : 'document.pdf')),
        Subject: document.Subject ?? 'Αποστολή Εγγράφου',
        Comments: document.Comments || '',
        AuthorName: document.AuthorName || '',
        RecipientSectorCodes: recipientCodes,
        OrgChartVersion: orgChartVersion,
        LocalSectorProtocolNo:
            document.LocalProtocolNo ||
            protocolSEQ(
                process.env.PROTOCOL_CONNECTION_STRING
                    ? process.env.PROTOCOL_CONNECTION_STRING
                    : '',
                process.env.PROTOCOL_RESET
                    ? (process.env.PROTOCOL_RESET as any)
                    : 'innumerable',
            ),
        LocalSectorProtocolDate:
            document.LocalProtocolDate || new Date().toISOString(),
        IsFinal: documents.length === 1,
    };
    if (!FirstDocumentRequest.FileName) throw new Error('FileName is required');
    if (!FirstDocumentRequest.Subject) throw new Error('Subject is required');
    if (
        !FirstDocumentRequest.RecipientSectorCodes ||
        FirstDocumentRequest.RecipientSectorCodes.length === 0
    )
        throw new Error('RecipientSectorCodes is required');

    const documentData = fileToBuffer(document.FileDataOrPath);
    const receipt = await sendDocument(
        token,
        documentData,
        FirstDocumentRequest,
    );

    //if there are more documents then send them as attachments
    if (documents.length > 1) {
        const response = [receipt];
        const documentProtocolNo = receipt.DocumentProtocolNo;
        for (let i = 1; i < documents.length; i++) {
            const document = documents[i];
            const AttachmentRequest: SendAttachmentRequest = {
                FileName:
                    '' +
                    (document.FileName ||
                        (typeof document.FileDataOrPath === 'string'
                            ? (document.FileDataOrPath as string)
                                  .split('/')
                                  .pop()
                            : 'attachment' + i + '.pdf')), //Filenames must be unique
                Comments: document.Comments || '',
                IsFinal: i === documents.length - 1,
            };
            if (!AttachmentRequest.FileName)
                throw new Error('FileName is required');
            const attachmentData = fileToBuffer(document.FileDataOrPath);
            const attachmentResp = await sendDocument(
                token,
                attachmentData,
                AttachmentRequest,
                true,
                documentProtocolNo,
            );
            response.push(attachmentResp);
        }
        return response;
    }

    return [receipt];
};

export default posToShde;
