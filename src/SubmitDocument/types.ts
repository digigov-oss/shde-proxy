export interface SectorDepartmentResource {
    //TODO na do ti einai
    SectorCode: number;
    DepartmentCode: number;
}

export interface Link {
    Rel?: string;
    Href?: string;
    Method?: string;
}

export interface SendDocumentRequest {
    FileName: string; //Όνομα αρχείου
    Subject: string; //Θέμα
    Comments?: string; //Σχόλια εγγράφου
    Category?: number; //Κατηγορία
    Classification?: number; //Εμπιστευτικότητα 0: Γενικό 1: Εμπιστευτικό
    AuthorName?: string; //Όνομα Συντάκτη
    DueDate?: string; //Ημερομηνία Προθεσμίας
    DiavgiaId?: string; //ΑΔΑ
    KimdisId?: string; //Αριθμός στο ΚΗΜΔΗΣ
    RelatedDocumentType?: number; //Τύπος Συσχέτισης 0: Καμία 1: Γενική 2: Απάντηση
    RelatedDocumentProtocolNo?: string; //Σχετικό Έγγραφο - Αρ. Πρωτ. ΣΔΔΔ
    MetadataJson?: string; //Επιπλέον metatadata (σε JSON μορφή) πχ. { "MetadataName" : "MetadataValue" }
    SenderSectorDirectorate?: string; //Διεύθυνση Φορέα Αποστολέα
    SenderSectorDepartment?: string; //Τμήμα Φορέα Αποστολέα
    RecipientSectorCodes: number[]; //Παραλήπτες φορείς (Κωδικοί Φορέων)
    RecipientSectorDepartments?: SectorDepartmentResource[]; //Παραλήπτες - Λίστα κωδικών τμημάτων από οργανόγραμμα φορέα
    CCSectorCodes?: number[]; //Κοινοποιημένοι φορείς (Κωδικοί Φορέων)
    CCSectorDepartments?: SectorDepartmentResource[]; //Κοινοποιημένοι Παραλήπτες - Λίστα κωδικών τμημάτων από οργανόγραμμα φορέα
    OrgChartVersion: number; //Αριθμός Έκδοσης Οργανογράμματος
    LocalSectorProtocolNo: string; //Αριθμός Πρωτοκόλλου Τοπικού ΣΗΔΕ
    LocalSectorProtocolDate: string; //Ημερομηνία Πρωτοκόλλησης Τοπικού ΣΗΔΕ
    VersionComments?: string; //Σχόλια έκδοσης
    IsFinal: boolean; //true προκειμένου να αποσταλεί το κεντρικό έγγραφο χωρίς συνημμένα αρχεία, ειδάλλως false
}

export interface SendAttachmentRequest {
    FileName: string; //Όνομα αρχείου
    Comments?: string; //Σχόλια εγγράφου
    IsFinal: boolean; //true εάν πρόκειται για το τελευταίο έγγραφο που θα επισυναφθεί - το κεντρικό έγγραφο θα αποσταλεί, ειδάλλως false
}

export interface ReceiptResource {
    ReceiptId: string; //Μοναδικός Αριθμός Αποδεικτικού Παραλαβής
    ReceiptDate: string; //Ημερομηνία Αποδεκτικού Παραλαβής
    DocumentProtocolNo: string; //Αριθμός Πρωτοκόλλου συσχετιζόμενου εγγράφου
    VersionNumber: number; //Αριθμός Έκδοσης συσχετιζόμενου εγγράφου
    Type: number; //Τύπος Αποδεικτικού Παραλαβής 1: Νέο Έγγραφο 2: Αλλαγή Εγγράφου 3: Αλλαγή Κατάστασης Εγγράφου 4: Παραλαβή Εγγράφου 5: Αποστολή Εγγράφου = ['1', '2', '3', '4', '5'],
    OriginatorId: number; //Αναγνωριστικό αποστολέα - 0: ΣΔΔΔ ή κωδικός φορέα
    RecipientId: number; //Αναγνωριστικό παραλήπτη - 0: ΣΔΔΔ ή κωδικός φορέα
    LocalReceiptId: string; //Αρ. Αποδεικτικού Παραλαβής ή Αριθμός Πρωτοκόλλου Εγγράφου στο Τοπικό ΣΗΔΕ του Φορέα
    Links: Link[];
}

export interface AttachmentResource {
    AttachmentId: string; //Αρ. Πρωτοκόλλου Συνημμένου Αρχείου ,
    DocumentProtocolNo: string; // Αριθμός Πρωτοκόλλου Εγγράφου ,
    VersionNumber: number; // Αριθμός έκδοσης αρχείου ,
    FileName: string; // Όνομα αρχείου ,
    DateReceived: string; // Ημερομηνία Παραλαβής ,
    Comments?: string; // Σχόλια / Παρατηρήσεις ,
    Hash?: string; // Hash Αρχείου (SHA-2 512) ,
    IsFinal?: boolean; // Εάν πρόκειται για το τελευταίο συνημμένο αρχείο του συγκεκριμένου Εγγράφου ,
    Links?: Link[]; // Σύνδεσμοι
}

export interface SideErrorActionResult {
    SideTrackingId: string; // Αναγνωριστικό Συσχέτισης ΣΗΔΕ ,
    HttpStatus: number; // HTTP Status Code
    ErrorCode: string; // Κωδικός Σφάλματος ,
    ErrorMessage: string | string[]; // Μήνυμα Λάθους ή Λίστα μηνυμάτων λάθους
}

export interface Document {
    FileDataOrPath: string | Uint8Array; //Δεδομένα αρχείου σε Uint8Array ή path στο αρχείο
    FileName?: string; //Όνομα αρχείου, αν δεν έχει δοθεί θα προκύψει από το path, πρέπει να δοθει αν το FileDataOrPath είναι Uint8Array
    Subject?: string; //Θέμα   it is required for first document! you can ommit for attachments
    Comments?: string; //Σχόλια εγγράφου
    AuthorName?: string; //Όνομα Συντάκτη
    LocalProtocolNo?: string; //Αριθμός Πρωτοκόλλου Τοπικού ΣΗΔΕ
    LocalProtocolDate?: string; //Ημερομηνία Πρωτοκόλλησης Τοπικού ΣΗΔΕ
}

export type Response =
    | ReceiptResource
    | AttachmentResource
    | SideErrorActionResult;
