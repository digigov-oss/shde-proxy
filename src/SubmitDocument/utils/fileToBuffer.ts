import fs from 'fs';

export const fileToBuffer = (fileDataOrPath: Uint8Array | string): Buffer => {
    let fileData: Buffer = Buffer.from('');
    if (fileDataOrPath instanceof Uint8Array)
        fileData = Buffer.from(fileDataOrPath); //convert Uint8Array to Buffer
    else if (typeof fileDataOrPath === 'string')
        fileData = fs.readFileSync(fileDataOrPath);
    else throw new Error('FileDataOrPath is required');
    return fileData;
};

export default fileToBuffer;
