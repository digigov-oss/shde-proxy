import { Organization, ChildNode } from './types';

export const flatten = (ChildNodes: ChildNode[]): Organization[] => {
    return ChildNodes.reduce((acc: Organization[], node: any) => {
        const { ChildNodes, ...rest } = node;
        return [
            ...acc,
            rest,
            ...flatten(ChildNodes).map((n: any) => ({
                ...n,
                ParentCode: rest.Code,
            })),
        ];
    }, []);
};
