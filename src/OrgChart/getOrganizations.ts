import axios from 'axios';
import { Organization } from './types';
import { flatten } from './utils';

export const getOrganizations = async (
    token: string,
    config?: {
        apiBase?: string;
    },
): Promise<Organization[]> => {
    const apiBase = config?.apiBase ?? process.env.KSHDE_API_BASE;
    const response = await axios.get(`${apiBase}/orgchart`, {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
    const organizations = flatten(response.data.RootNode.ChildNodes);
    return organizations;
};

export default getOrganizations;
