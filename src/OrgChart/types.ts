//Is the flattened version of the OrgChart
export type Organization = {
    Code: number;
    Name: string;
    NameEnglish: string | null;
    IsSDDDNode: boolean;
    ParentCode: number | null;
    IsActive: boolean;
};

//Is the response body from the api https://sdddsp.mindigital-shde.gr/api/v1/orgchart
export type OrgChart = {
    Version: number;
    RootNode: ChildNode;
};

//it is Organization omit ParentCode and add ChildNodes
export type ChildNode = Omit<Organization, 'ParentCode'> & {
    ChildNodes: ChildNode[];
};

export type Columns = {
    Code: string;
    Name: string;
    NameEnglish: string;
    IsSDDDNode: string;
    ParentCode: string;
    IsActive: string;
};

export type DatabaseSettings = {
    tableName?: string;
    columns?: Columns;
};
