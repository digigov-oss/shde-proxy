import getToken from '../Authenticate/getToken';
import getOrganizations from './getOrganizations';
import OrgChartDB from './db/orgchart';
import { DatabaseSettings } from './types';

export async function getOrgChart(
    dryRun: boolean = false,
    dbSettings: DatabaseSettings = {},
) {
    //check if the environment variables are set
    if (
        !dryRun &&
        (!process.env.ORGANIZATIONS_POSTGRES_USER ||
            !process.env.ORGANIZATIONS_POSTGRES_PASSWORD ||
            !process.env.ORGANIZATIONS_POSTGRES_DB ||
            !process.env.ORGANIZATIONS_POSTGRES_HOST ||
            !process.env.ORGANIZATIONS_POSTGRES_PORT)
    ) {
        console.log(
            'Please set the environment variables ORGANIZATIONS_POSTGRES_USER, ORGANIZATIONS_POSTGRES_PASSWORD, ORGANIZATIONS_POSTGRES_DB, ORGANIZATIONS_POSTGRES_HOST, ORGANIZATIONS_POSTGRES_PORT',
        );
        return null;
    }
    try {
        const token = await getToken();
        const organizations = await getOrganizations(token);
        if (dryRun) return organizations;

        const POSTGRES_USER = process.env.ORGANIZATIONS_POSTGRES_USER;
        const POSTGRES_PASSWORD = process.env.ORGANIZATIONS_POSTGRES_PASSWORD;
        const POSTGRES_DB = process.env.ORGANIZATIONS_POSTGRES_DB;
        const POSTGRES_HOST = process.env.ORGANIZATIONS_POSTGRES_HOST;
        const POSTGRES_PORT = process.env.ORGANIZATIONS_POSTGRES_PORT;
        const orgChartDB = new OrgChartDB(
            `postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}`,
            dbSettings,
        );
        return orgChartDB.saveOrganizations(organizations);
    } catch (error) {
        console.log(error);
    }
    return null;
}

export default getOrgChart;
