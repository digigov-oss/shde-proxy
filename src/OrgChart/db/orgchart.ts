//To check this you have to have a PostgreSQL database installed and running.
//The database must be configured to allow connections from the host that the application is running on.
//The database must also have the following table:
//table organizations
//code: integer;
//name: varchar(255);
//nameEnglish: varchar(255);
//isSDDDNode: boolean;
//parentCode: integer;
//isActive: boolean;
// if you do not provide the table name, the module will create it for you.

//You can use Docker to run PostgreSQL for your tests.
//`docker run -d --name organizations-postgres -e POSTGRES_PASSWORD=organizations -p 5432:5432 postgres`
//`docker exec -it organizations-postgres psql -U postgres -h localhost -c "CREATE DATABASE organizations;"`
//`docker exec -it organizations-postgres psql -U postgres -h localhost -c "CREATE TABLE organizations (code integer, name varchar(255), nameEnglish varchar(255), isSDDDNode boolean, parentCode integer, isActive boolean);"`

//You can also map the fields of already existent table to the fields in the audit log.

import Client from 'pg-native';
import { Organization, Columns, DatabaseSettings } from '../types';

export default class OrgChartDB {
    #table: string;
    #columns: Columns;
    #client: any;

    constructor(
        connectionString: string = '',
        dbSettings: DatabaseSettings = {},
    ) {
        this.#table = dbSettings.tableName || 'organizations';
        this.#columns = dbSettings.columns || {
            Code: 'Code',
            Name: 'Name',
            NameEnglish: 'NameEnglish',
            IsSDDDNode: 'IsSDDDNode',
            ParentCode: 'ParentCode',
            IsActive: 'IsActive',
        };
        this.#client = new Client();
        this.#client.connectSync(connectionString);
        if (!dbSettings.tableName) {
            this.#client.querySync(
                'CREATE TABLE IF NOT EXISTS ' +
                    this.#table +
                    ' (' +
                    this.#columns.Code +
                    ' integer UNIQUE, ' +
                    this.#columns.Name +
                    ' varchar(255), ' +
                    this.#columns.NameEnglish +
                    ' varchar(255), ' +
                    this.#columns.IsSDDDNode +
                    ' boolean, ' +
                    this.#columns.ParentCode +
                    ' integer, ' +
                    this.#columns.IsActive +
                    ' boolean);',
            );
        }
    }

    saveOrganizations(organizations: Organization[]) {
        const query = `INSERT INTO ${
            this.#table
        } (Code, Name, NameEnglish, IsSDDDNode, ParentCode, IsActive) VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT (Code) DO UPDATE SET Name = $2, NameEnglish = $3, IsSDDDNode = $4, ParentCode = $5, IsActive = $6`;
        for (const organization of organizations) {
            const Name = organization.Name || '';
            const NameEnglish = organization.NameEnglish || '';
            const IsSDDDNode = organization.IsSDDDNode || false;
            const ParentCode = organization.ParentCode || null;
            const IsActive = organization.IsActive || false;
            this.#client.querySync(query, [
                organization.Code,
                Name,
                NameEnglish,
                IsSDDDNode,
                ParentCode,
                IsActive,
            ]);
        }
        return this.getOrganizations();
    }

    getOrganizations() {
        const query = `SELECT * FROM ${this.#table}`;
        const result = this.#client.querySync(query);
        return result;
    }
}
