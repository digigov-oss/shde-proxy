import axios from 'axios';

const authenticate = async (
    apiBase: string,
    sectorCode: string,
    clientId: string,
    clientSecret: string,
) => {
    const response = await axios.post(`${apiBase}/authenticate`, {
        SectorCode: parseInt(sectorCode),
        ClientId: clientId,
        ClientSecret: clientSecret,
    });
    return response.data;
};

export const getToken = async (config?: {
    apiBase?: string;
    sectorCode?: string;
    clientId?: string;
    clientSecret?: string;
}) => {
    const apiBase = config?.apiBase ?? process.env.KSHDE_API_BASE;
    if (!apiBase) {
        throw new Error('Missing KSHDE api base url');
    }
    const sectorCode = config?.sectorCode ?? process.env.KSHDE_SECTOR_CODE;
    if (!sectorCode) {
        throw new Error('Missing KSHDE sector code');
    }
    const clientId = config?.clientId ?? process.env.KSHDE_CLIENT_ID;
    if (!clientId) {
        throw new Error('Missing KSHDE client id');
    }
    const clientSecret =
        config?.clientSecret ?? process.env.KSHDE_CLIENT_SECRET;
    if (!clientSecret) {
        throw new Error('Missing KSHDE client secret');
    }
    const response = await authenticate(
        apiBase,
        sectorCode,
        clientId,
        clientSecret,
    );
    return response.AccessToken;
};

export default getToken;
