// NODE_ENV=development node --experimental-specifier-resolution=node --experimental-modules --no-warnings --loader ts-node/esm ./sendDocToKEPBackoffice.x.ts
// send to 1234567  (KEP Backoffice)
import dotenv from "dotenv";
//set to parent directory // __dirname is not defined in ES module scope
dotenv.config({path:'../.env'});

import {posToShde} from '../src';
import pdfGen from '../tools/pdfGen';
import fs from 'fs';

//generate a test pdf file
pdfGen(new Date().toISOString().slice(0, 10),'/tmp/test.pdf');

const documents = [
    {
        FileDataOrPath:'/tmp/test.pdf', //if FileName is omitted then the last part of FileDataOrPath is used, if FileDataOrPath is string, else the generic name 'document.pdf' is used
        Subject: 'test', //required for the first document only, if omitted then default value is used
        LocalProtocolNo: Math.random().toString(36).substring(7), //if omitted then postgreSQL sequence (as protocol) is used, in that case you have to set enviroment variables PGUSER, PGPASSWORD, PGDATABASE, PGHOST, PGPORT
    },
    {
        FileName: 'attachment.pdf', //if omitted then the last part of FileDataOrPath is used, if FileDataOrPath is string, else the generic name 'attachmentX.pdf' is used, FileNames must be unique
        FileDataOrPath: new Uint8Array(fs.readFileSync('/tmp/test.pdf')),//you can use Uint8Array as well
    },
    {
        FileName: 'test.pdf', //Filenames must be unique, so this will return an SideErrorActionResult (500), api is not very informative about the cause of the errors 
        FileDataOrPath: '/tmp/test.pdf', 
    },
];

const recipientCode = 1234567; // or array of recipient codes.
const receipt = await posToShde(documents, recipientCode);
//receipt is an array with ReceiptResource at [0] and may one or more AttachmentResource's and or SideErrorActionResult's
console.log(receipt);
