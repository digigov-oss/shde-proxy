//NODE_ENV=development node --experimental-specifier-resolution=node --experimental-modules --no-warnings --loader ts-node/esm ./listOrganizations.x.ts
// Purpose: List all organizations without updating the database.

import dotenv from "dotenv";
dotenv.config({path:'../.env'});
import { getOrgChart } from "../src";

const orgChart = await getOrgChart(true); //true means do not update the database
console.log(orgChart);
