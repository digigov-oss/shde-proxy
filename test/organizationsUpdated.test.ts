import getOrgChart from "../src/OrgChart";

import dotenv from "dotenv";
const envpath=__dirname+'/../.env';
dotenv.config({path:envpath});

describe("getOrgChart-test", () => {
//use getOrganizations to get the organizations saved on db
    it("should return an array of organizations from database",  async () => {
        const organizations =  await getOrgChart();
        expect(organizations).toBeDefined();
        //check if the organizations are an array
        expect(Array.isArray(organizations)).toBe(true);
        //check if the organizations in not empty
        expect(organizations.length).toBeGreaterThan(0);
    }
    );
}
);
