import OrgChartDB from "../src/OrgChart/db/orgchart";
import dotenv from "dotenv";
const envpath=__dirname+'/../.env';
dotenv.config({path:envpath});

describe("OrgChartDB-test", () => {
//use getOrganizations to get the organizations saved on db
    it("should return an array of organizations from database",  () => {
        const POSTGRES_USER=process.env.ORGANIZATIONS_POSTGRES_USER
        const POSTGRES_PASSWORD=process.env.ORGANIZATIONS_POSTGRES_PASSWORD
        const POSTGRES_DB=process.env.ORGANIZATIONS_POSTGRES_DB
        const POSTGRES_HOST=process.env.ORGANIZATIONS_POSTGRES_HOST
        const POSTGRES_PORT=process.env.ORGANIZATIONS_POSTGRES_PORT
        const orgChartDB = new OrgChartDB(`postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}`);
        const organizations =  orgChartDB.getOrganizations();
        expect(organizations).toBeDefined();
    }
    );
}
);

