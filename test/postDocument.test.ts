import type {ReceiptResource,Response} from '../src/SubmitDocument/types';
import posToShde from '../src/SubmitDocument';
import pdfGen from '../tools/pdfGen';
import dotenv from "dotenv";
const envpath=__dirname+'/../.env';
dotenv.config({path:envpath});

describe('postDocument-test', () => {
    it('should post a document', async () => {
        //generate a test pdf file
        pdfGen(new Date().toISOString().slice(0, 10),'/tmp/test.pdf');
        const documents = [
            {   FileDataOrPath:'/tmp/test.pdf',
                FileName: 'test.pdf',
                Subject: 'test',
                Comments: 'test',
                AuthorName: 'test'
            },
        ];
        const recipientCode = 1234567;
        const receipt:Response[] = await posToShde(documents, recipientCode) as ReceiptResource[];
        expect(receipt).toBeDefined();
        expect(Array.isArray(receipt)).toBe(true);
        expect(receipt.length).toBeGreaterThan(0);
        const docReceipt = receipt[0] as ReceiptResource;
        expect(docReceipt.ReceiptId).toBeDefined();
        expect(docReceipt.ReceiptDate).toBeDefined();
        expect(docReceipt.DocumentProtocolNo).toBeDefined();
        expect(docReceipt.VersionNumber).toBeDefined();
        expect(docReceipt.Type).toBeDefined();
        expect(docReceipt.OriginatorId).toBeDefined();
        expect(docReceipt.RecipientId).toBeDefined();
        expect(docReceipt.LocalReceiptId).toBeDefined();
        expect(docReceipt.Links).toBeDefined();
    });
});

