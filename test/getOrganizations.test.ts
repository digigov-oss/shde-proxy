// use jest to test getOrganizations
import getOrganizations from "../src/OrgChart/getOrganizations";
import getToken from "../src/Authenticate/getToken";
import dotenv from "dotenv";
const envpath=__dirname+'/../.env';
dotenv.config({path:envpath});

describe("getOrganizations-test", () => {
    it("should return an array of organizations", async () => {
        const token = await getToken();
        const organizations = await getOrganizations(token);
        expect(organizations).toBeDefined();
        expect(organizations.length).toBeGreaterThan(0);
    });
}

);

