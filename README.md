# Shde Proxy

## Description
kshde simplifier, use one component to send files to destination 

## Installation

### PostgreSQL instalation
You have to install the native libpg library for Postgresql.

* On macOS: brew install libpq

* On Ubuntu/Debian: apt-get install libpq-dev g++ make

* On RHEL/CentOS: yum install postgresql-devel

You can use Docker to run PostgreSQL for your tests.

`docker run -d --name organizations-postgres -e POSTGRES_PASSWORD=organizations -p 5432:5432 postgres`

`docker exec -it organizations-postgres psql -U postgres -h localhost -c "CREATE DATABASE organizations;"`

`docker exec -it organizations-postgres psql -U postgres -h localhost -c "CREATE TABLE organizations (Code integer UNIQUE, Name varchar(255), NameEnglish varchar(255), IsSDDDNode boolean, ParentCode integer, IsActive boolean);"`

> Mac OS ==> Caveats
>
>libpq is keg-only, which means it was not symlinked into /usr/local,
because conflicts with postgres formula. So you need to have libpq first in your PATH before using `yarn install` or `npm install`
 > * run: `echo 'export PATH="/usr/local/opt/libpq/bin:$PATH"' >> ~/.zshrc`

## Get Organizations
The organizations must be retrieved from time to time from the source and  saved to get the reciepients codes.

* By default we use PostgreSQL but you can call the function `getOrgChart(true)` to dry run without saving the organizations on database, then you can save them to your database.

 You can use a cron job or scheduled task with a script like the one provided in `tools/updateDB.sh` to update the organizations in your database. 
 
 The connection string for the database is constructed from environment variables:

* ORGANIZATIONS_POSTGRES_USER
* ORGANIZATIONS_POSTGRES_PASSWORD
* ORGANIZATIONS_POSTGRES_DB
* ORGANIZATIONS_POSTGRES_HOST
* ORGANIZATIONS_POSTGRES_PORT

The table name 'organizations' is created if it does not already exist, but you can map an existing table and columns to your own database.

example:

```JavaScript
const dbSettings = {
    tableName: 'shdeOrgs',
    columns: {
            Code: "ID",
            Name: "Name",
            NameEnglish: "ForeignN",
            IsSDDDNode: "IsSDDDNode",
            ParentCode: "ParentID",
            IsActive: "IsActive"
    }
}
const orgs = await getOrgChart(false, dbSettings)
```

## Submit Document
Use the function `posToShde` to send a document with optiolaly attachments to shde service. 
The function accepts an array of objects on property `documents` with the following attributes:

    FileDataOrPath: string | Uint8Array; //File data in Uint8Array or path to file (required)
    FileName?: string; // Filename, if not given will be derived from path, must be given if FileDataOrPath is Uint8Array
    Subject?: string; //it is required for first document! you can ommit for attachments
    Comments?: string; //Document comments (optional)
    AuthorName?: string; //Author Name (optional)
    LocalProtocolNo?: string; //Protocol Number of the Local SHDE, if ommited will be generated
    LocalProtocolDate?: string; //if ommited will be generated

>Only the first document must have a subject, the rest is attachents.

>If `LocalProtocolNo` is given then `LocalProtocolDate` must be given too.
>
>If not given then `LocalProtocolNo` will be generated as sequence on PostgreSQL, 
>* you can connect by setting the environment variables `PGPORT`, `PGHOST`, `PGUSER`, `PGPASSWORD`, `PGDATABASE` or by setting the connection string on environment variable `PROTOCOL_CONNECTION_STRING`. Also you can set the reset period by setting the environment variable `PROTOCOL_RESET` to any of "daily" | "monthly" | "yearly" | "innumerable".

and reciptient codes array or a single code at the property `recipientCode`

    RecipientCode: number | number[]; //Recipient Code or array of Recipient Codes you get from getOrgChart

also may need to set the version of chart by seting the `orgChartVersion` property (default is 1)

Look at `examples/sendDocToKEPBackofice.x.ts` for more info.

* The maximum file size is 30MB, for each document/attachment. The total size of all attachments must not exceed 2GB.

### Returns
returns an object like 
```
[
  {
    ReceiptId: '2023xxxx',
    ReceiptDate: '2023-06-15T19:24:56.163',
    DocumentProtocolNo: '202309374200000XXXXX',
    VersionNumber: 1,
    Type: 1,
    OriginatorId: 0,
    RecipientId: 93742,
    LocalReceiptId: null,
    Links: [ [Object], [Object] ]
  },
  {
    AttachmentId: '202309374200000XXXX-1',
    DocumentProtocolNo: '202309374200000XXXXX',
    VersionNumber: 1,
    FileName: 'attachment.pdf',
    DateReceived: '2023-06-15T19:24:56.52',
    Comments: '',
    Hash: '95CB34374.....',
    IsFinal: false,
    Links: [ [Object], [Object], [Object], [Object], [Object] ]
  },
  {
    SideTrackingId: 'd54d5270-xxxx',
    HttpStatus: 500, 
    ErrorCode: null, * The api is not very informative about the errors
    ErrorMessage: 'A server-side error occurred. In case this issue persists, please contact helpdesk.' 
  }
]
```

* Please note, if any error ocurrs, on any of the attachments, the whole document will be rejected and not reach the destination. 

* The documentation for the kshde service can be found at https://support.mindigital-shde.gr/manuals/. However, it is written in Greek and may be outdated and contain errors.