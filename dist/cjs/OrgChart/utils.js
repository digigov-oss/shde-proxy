"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.flatten = void 0;
const flatten = (ChildNodes) => {
    return ChildNodes.reduce((acc, node) => {
        const { ChildNodes } = node, rest = __rest(node, ["ChildNodes"]);
        return [
            ...acc,
            rest,
            ...(0, exports.flatten)(ChildNodes).map((n) => (Object.assign(Object.assign({}, n), { ParentCode: rest.Code }))),
        ];
    }, []);
};
exports.flatten = flatten;
