"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getOrganizations = void 0;
const axios_1 = __importDefault(require("axios"));
const utils_1 = require("./utils");
const getOrganizations = (token, config) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const apiBase = (_a = config === null || config === void 0 ? void 0 : config.apiBase) !== null && _a !== void 0 ? _a : process.env.KSHDE_API_BASE;
    const response = yield axios_1.default.get(`${apiBase}/orgchart`, {
        headers: {
            Authorization: `Bearer ${token}`,
        },
    });
    const organizations = (0, utils_1.flatten)(response.data.RootNode.ChildNodes);
    return organizations;
});
exports.getOrganizations = getOrganizations;
exports.default = exports.getOrganizations;
