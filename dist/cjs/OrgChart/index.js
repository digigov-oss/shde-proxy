"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getOrgChart = void 0;
const getToken_1 = __importDefault(require("../Authenticate/getToken"));
const getOrganizations_1 = __importDefault(require("./getOrganizations"));
const orgchart_1 = __importDefault(require("./db/orgchart"));
function getOrgChart(dryRun = false, dbSettings = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        //check if the environment variables are set
        if (!dryRun &&
            (!process.env.ORGANIZATIONS_POSTGRES_USER ||
                !process.env.ORGANIZATIONS_POSTGRES_PASSWORD ||
                !process.env.ORGANIZATIONS_POSTGRES_DB ||
                !process.env.ORGANIZATIONS_POSTGRES_HOST ||
                !process.env.ORGANIZATIONS_POSTGRES_PORT)) {
            console.log('Please set the environment variables ORGANIZATIONS_POSTGRES_USER, ORGANIZATIONS_POSTGRES_PASSWORD, ORGANIZATIONS_POSTGRES_DB, ORGANIZATIONS_POSTGRES_HOST, ORGANIZATIONS_POSTGRES_PORT');
            return null;
        }
        try {
            const token = yield (0, getToken_1.default)();
            const organizations = yield (0, getOrganizations_1.default)(token);
            if (dryRun)
                return organizations;
            const POSTGRES_USER = process.env.ORGANIZATIONS_POSTGRES_USER;
            const POSTGRES_PASSWORD = process.env.ORGANIZATIONS_POSTGRES_PASSWORD;
            const POSTGRES_DB = process.env.ORGANIZATIONS_POSTGRES_DB;
            const POSTGRES_HOST = process.env.ORGANIZATIONS_POSTGRES_HOST;
            const POSTGRES_PORT = process.env.ORGANIZATIONS_POSTGRES_PORT;
            const orgChartDB = new orgchart_1.default(`postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}`, dbSettings);
            return orgChartDB.saveOrganizations(organizations);
        }
        catch (error) {
            console.log(error);
        }
        return null;
    });
}
exports.getOrgChart = getOrgChart;
exports.default = getOrgChart;
