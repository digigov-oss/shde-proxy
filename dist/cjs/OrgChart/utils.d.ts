import { Organization, ChildNode } from './types';
export declare const flatten: (ChildNodes: ChildNode[]) => Organization[];
