"use strict";
//To check this you have to have a PostgreSQL database installed and running.
//The database must be configured to allow connections from the host that the application is running on.
//The database must also have the following table:
//table organizations
//code: integer;
//name: varchar(255);
//nameEnglish: varchar(255);
//isSDDDNode: boolean;
//parentCode: integer;
//isActive: boolean;
// if you do not provide the table name, the module will create it for you.
var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, state, value, kind, f) {
    if (kind === "m") throw new TypeError("Private method is not writable");
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a setter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot write private member to an object whose class did not declare it");
    return (kind === "a" ? f.call(receiver, value) : f ? f.value = value : state.set(receiver, value)), value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, state, kind, f) {
    if (kind === "a" && !f) throw new TypeError("Private accessor was defined without a getter");
    if (typeof state === "function" ? receiver !== state || !f : !state.has(receiver)) throw new TypeError("Cannot read private member from an object whose class did not declare it");
    return kind === "m" ? f : kind === "a" ? f.call(receiver) : f ? f.value : state.get(receiver);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _OrgChartDB_table, _OrgChartDB_columns, _OrgChartDB_client;
Object.defineProperty(exports, "__esModule", { value: true });
//You can use Docker to run PostgreSQL for your tests.
//`docker run -d --name organizations-postgres -e POSTGRES_PASSWORD=organizations -p 5432:5432 postgres`
//`docker exec -it organizations-postgres psql -U postgres -h localhost -c "CREATE DATABASE organizations;"`
//`docker exec -it organizations-postgres psql -U postgres -h localhost -c "CREATE TABLE organizations (code integer, name varchar(255), nameEnglish varchar(255), isSDDDNode boolean, parentCode integer, isActive boolean);"`
//You can also map the fields of already existent table to the fields in the audit log.
const pg_native_1 = __importDefault(require("pg-native"));
class OrgChartDB {
    constructor(connectionString = '', dbSettings = {}) {
        _OrgChartDB_table.set(this, void 0);
        _OrgChartDB_columns.set(this, void 0);
        _OrgChartDB_client.set(this, void 0);
        __classPrivateFieldSet(this, _OrgChartDB_table, dbSettings.tableName || 'organizations', "f");
        __classPrivateFieldSet(this, _OrgChartDB_columns, dbSettings.columns || {
            Code: 'Code',
            Name: 'Name',
            NameEnglish: 'NameEnglish',
            IsSDDDNode: 'IsSDDDNode',
            ParentCode: 'ParentCode',
            IsActive: 'IsActive',
        }, "f");
        __classPrivateFieldSet(this, _OrgChartDB_client, new pg_native_1.default(), "f");
        __classPrivateFieldGet(this, _OrgChartDB_client, "f").connectSync(connectionString);
        if (!dbSettings.tableName) {
            __classPrivateFieldGet(this, _OrgChartDB_client, "f").querySync('CREATE TABLE IF NOT EXISTS ' +
                __classPrivateFieldGet(this, _OrgChartDB_table, "f") +
                ' (' +
                __classPrivateFieldGet(this, _OrgChartDB_columns, "f").Code +
                ' integer UNIQUE, ' +
                __classPrivateFieldGet(this, _OrgChartDB_columns, "f").Name +
                ' varchar(255), ' +
                __classPrivateFieldGet(this, _OrgChartDB_columns, "f").NameEnglish +
                ' varchar(255), ' +
                __classPrivateFieldGet(this, _OrgChartDB_columns, "f").IsSDDDNode +
                ' boolean, ' +
                __classPrivateFieldGet(this, _OrgChartDB_columns, "f").ParentCode +
                ' integer, ' +
                __classPrivateFieldGet(this, _OrgChartDB_columns, "f").IsActive +
                ' boolean);');
        }
    }
    saveOrganizations(organizations) {
        const query = `INSERT INTO ${__classPrivateFieldGet(this, _OrgChartDB_table, "f")} (Code, Name, NameEnglish, IsSDDDNode, ParentCode, IsActive) VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT (Code) DO UPDATE SET Name = $2, NameEnglish = $3, IsSDDDNode = $4, ParentCode = $5, IsActive = $6`;
        for (const organization of organizations) {
            const Name = organization.Name || '';
            const NameEnglish = organization.NameEnglish || '';
            const IsSDDDNode = organization.IsSDDDNode || false;
            const ParentCode = organization.ParentCode || null;
            const IsActive = organization.IsActive || false;
            __classPrivateFieldGet(this, _OrgChartDB_client, "f").querySync(query, [
                organization.Code,
                Name,
                NameEnglish,
                IsSDDDNode,
                ParentCode,
                IsActive,
            ]);
        }
        return this.getOrganizations();
    }
    getOrganizations() {
        const query = `SELECT * FROM ${__classPrivateFieldGet(this, _OrgChartDB_table, "f")}`;
        const result = __classPrivateFieldGet(this, _OrgChartDB_client, "f").querySync(query);
        return result;
    }
}
exports.default = OrgChartDB;
_OrgChartDB_table = new WeakMap(), _OrgChartDB_columns = new WeakMap(), _OrgChartDB_client = new WeakMap();
