import { Organization, DatabaseSettings } from '../types';
export default class OrgChartDB {
    #private;
    constructor(connectionString?: string, dbSettings?: DatabaseSettings);
    saveOrganizations(organizations: Organization[]): any;
    getOrganizations(): any;
}
