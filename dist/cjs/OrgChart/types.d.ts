export type Organization = {
    Code: number;
    Name: string;
    NameEnglish: string | null;
    IsSDDDNode: boolean;
    ParentCode: number | null;
    IsActive: boolean;
};
export type OrgChart = {
    Version: number;
    RootNode: ChildNode;
};
export type ChildNode = Omit<Organization, 'ParentCode'> & {
    ChildNodes: ChildNode[];
};
export type Columns = {
    Code: string;
    Name: string;
    NameEnglish: string;
    IsSDDDNode: string;
    ParentCode: string;
    IsActive: string;
};
export type DatabaseSettings = {
    tableName?: string;
    columns?: Columns;
};
