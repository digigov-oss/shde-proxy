"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fileToBuffer = exports.OrgChartDB = exports.posToShde = exports.getToken = exports.getOrganizations = exports.getOrgChart = void 0;
const config_json_1 = __importDefault(require("./config.json"));
if (!process.env.KSHDE_API_BASE) {
    console.info('KSHDE_API_BASE is not set, trying to get it from config.json');
    if (process.env.NODE_ENV === 'development')
        process.env.KSHDE_API_BASE = config_json_1.default.test;
    else
        process.env.KSHDE_API_BASE = config_json_1.default.prod;
}
//export functions
var OrgChart_1 = require("./OrgChart");
Object.defineProperty(exports, "getOrgChart", { enumerable: true, get: function () { return OrgChart_1.getOrgChart; } });
var getOrganizations_1 = require("./OrgChart/getOrganizations");
Object.defineProperty(exports, "getOrganizations", { enumerable: true, get: function () { return getOrganizations_1.getOrganizations; } });
var getToken_1 = require("./Authenticate/getToken");
Object.defineProperty(exports, "getToken", { enumerable: true, get: function () { return getToken_1.getToken; } });
var SubmitDocument_1 = require("./SubmitDocument");
Object.defineProperty(exports, "posToShde", { enumerable: true, get: function () { return SubmitDocument_1.posToShde; } });
//export classes
var orgchart_1 = require("./OrgChart/db/orgchart");
Object.defineProperty(exports, "OrgChartDB", { enumerable: true, get: function () { return __importDefault(orgchart_1).default; } });
//export utils
var fileToBuffer_1 = require("./SubmitDocument/utils/fileToBuffer");
Object.defineProperty(exports, "fileToBuffer", { enumerable: true, get: function () { return fileToBuffer_1.fileToBuffer; } });
