export type PNRESETTYPES = 'daily' | 'monthly' | 'yearly' | 'innumerable';
declare const protocolSEQ: (connectionString?: string, pnreset?: PNRESETTYPES) => string;
export default protocolSEQ;
