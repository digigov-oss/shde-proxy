import type { Document } from '../types';
export declare const validateDocuments: (documents: Document[]) => void;
