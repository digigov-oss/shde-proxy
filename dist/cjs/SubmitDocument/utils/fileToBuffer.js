"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fileToBuffer = void 0;
const fs_1 = __importDefault(require("fs"));
const fileToBuffer = (fileDataOrPath) => {
    let fileData = Buffer.from('');
    if (fileDataOrPath instanceof Uint8Array)
        fileData = Buffer.from(fileDataOrPath); //convert Uint8Array to Buffer
    else if (typeof fileDataOrPath === 'string')
        fileData = fs_1.default.readFileSync(fileDataOrPath);
    else
        throw new Error('FileDataOrPath is required');
    return fileData;
};
exports.fileToBuffer = fileToBuffer;
exports.default = exports.fileToBuffer;
