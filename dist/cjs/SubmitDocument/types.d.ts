export interface SectorDepartmentResource {
    SectorCode: number;
    DepartmentCode: number;
}
export interface Link {
    Rel?: string;
    Href?: string;
    Method?: string;
}
export interface SendDocumentRequest {
    FileName: string;
    Subject: string;
    Comments?: string;
    Category?: number;
    Classification?: number;
    AuthorName?: string;
    DueDate?: string;
    DiavgiaId?: string;
    KimdisId?: string;
    RelatedDocumentType?: number;
    RelatedDocumentProtocolNo?: string;
    MetadataJson?: string;
    SenderSectorDirectorate?: string;
    SenderSectorDepartment?: string;
    RecipientSectorCodes: number[];
    RecipientSectorDepartments?: SectorDepartmentResource[];
    CCSectorCodes?: number[];
    CCSectorDepartments?: SectorDepartmentResource[];
    OrgChartVersion: number;
    LocalSectorProtocolNo: string;
    LocalSectorProtocolDate: string;
    VersionComments?: string;
    IsFinal: boolean;
}
export interface SendAttachmentRequest {
    FileName: string;
    Comments?: string;
    IsFinal: boolean;
}
export interface ReceiptResource {
    ReceiptId: string;
    ReceiptDate: string;
    DocumentProtocolNo: string;
    VersionNumber: number;
    Type: number;
    OriginatorId: number;
    RecipientId: number;
    LocalReceiptId: string;
    Links: Link[];
}
export interface AttachmentResource {
    AttachmentId: string;
    DocumentProtocolNo: string;
    VersionNumber: number;
    FileName: string;
    DateReceived: string;
    Comments?: string;
    Hash?: string;
    IsFinal?: boolean;
    Links?: Link[];
}
export interface SideErrorActionResult {
    SideTrackingId: string;
    HttpStatus: number;
    ErrorCode: string;
    ErrorMessage: string | string[];
}
export interface Document {
    FileDataOrPath: string | Uint8Array;
    FileName?: string;
    Subject?: string;
    Comments?: string;
    AuthorName?: string;
    LocalProtocolNo?: string;
    LocalProtocolDate?: string;
}
export type Response = ReceiptResource | AttachmentResource | SideErrorActionResult;
