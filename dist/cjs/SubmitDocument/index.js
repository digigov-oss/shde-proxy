"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.posToShde = void 0;
const getToken_1 = __importDefault(require("../Authenticate/getToken"));
const axios_1 = __importDefault(require("axios"));
const form_data_1 = __importDefault(require("form-data"));
const fileToBuffer_1 = require("./utils/fileToBuffer");
const validateDocuments_1 = require("./utils/validateDocuments");
const protocol_1 = __importDefault(require("./db/protocol"));
const stream_1 = require("stream");
const mime_1 = __importDefault(require("mime"));
const sendDocument = (token, document, documentMetaData, isAttachment = false, DocumentProtocolNo = '', config) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const apiBase = (_a = config === null || config === void 0 ? void 0 : config.apiBase) !== null && _a !== void 0 ? _a : process.env.KSHDE_API_BASE;
    if (!apiBase)
        throw new Error('KSHDE_API_BASE is not defined');
    const form = new form_data_1.default();
    const stream = new stream_1.Readable({
        read() {
            this.push(document);
            this.push(null);
        },
    });
    //form.append('DocumentContent', fs.createReadStream('/tmp/test.pdf'), { knownLength: fs.statSync('/tmp/test.pdf').size });
    form.append('DocumentContent', stream, {
        knownLength: document.length,
        filename: documentMetaData.FileName,
        contentType: '' + mime_1.default.getType(documentMetaData.FileName),
    });
    form.append('DocumentMetadata', JSON.stringify(documentMetaData));
    const headers = form.getHeaders();
    const apiURL = isAttachment
        ? apiBase + `/documents/${DocumentProtocolNo}/attachments`
        : apiBase + '/documents';
    try {
        const response = yield axios_1.default.post(apiURL, form, {
            headers: Object.assign(Object.assign({}, headers), { 'Content-Length': form.getLengthSync(), Authorization: `Bearer ${token}` }),
        });
        return response.data;
    }
    catch (error) {
        //if is error 400,403,404,500 then return the error
        if (error.response) {
            if (error.response.status === 400 ||
                error.response.status === 403 ||
                error.response.status === 404 ||
                error.response.status === 500) {
                return error.response.data;
            }
        }
        console.log(error);
        return {};
    }
});
const posToShde = (documents, recipientCode, orgChartVersion = 1, config) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    //validate documents
    (0, validateDocuments_1.validateDocuments)(documents); //throws error if validation fails
    //get token
    const token = yield (0, getToken_1.default)(config);
    //prepare SendDocumentRequest for the first document
    const document = documents[0];
    const recipientCodes = Array.isArray(recipientCode)
        ? recipientCode
        : [recipientCode];
    const FirstDocumentRequest = {
        FileName: '' +
            (document.FileName ||
                (typeof document.FileDataOrPath === 'string'
                    ? document.FileDataOrPath.split('/').pop()
                    : 'document.pdf')),
        Subject: (_b = document.Subject) !== null && _b !== void 0 ? _b : 'Αποστολή Εγγράφου',
        Comments: document.Comments || '',
        AuthorName: document.AuthorName || '',
        RecipientSectorCodes: recipientCodes,
        OrgChartVersion: orgChartVersion,
        LocalSectorProtocolNo: document.LocalProtocolNo ||
            (0, protocol_1.default)(process.env.PROTOCOL_CONNECTION_STRING
                ? process.env.PROTOCOL_CONNECTION_STRING
                : '', process.env.PROTOCOL_RESET
                ? process.env.PROTOCOL_RESET
                : 'innumerable'),
        LocalSectorProtocolDate: document.LocalProtocolDate || new Date().toISOString(),
        IsFinal: documents.length === 1,
    };
    if (!FirstDocumentRequest.FileName)
        throw new Error('FileName is required');
    if (!FirstDocumentRequest.Subject)
        throw new Error('Subject is required');
    if (!FirstDocumentRequest.RecipientSectorCodes ||
        FirstDocumentRequest.RecipientSectorCodes.length === 0)
        throw new Error('RecipientSectorCodes is required');
    const documentData = (0, fileToBuffer_1.fileToBuffer)(document.FileDataOrPath);
    const receipt = yield sendDocument(token, documentData, FirstDocumentRequest);
    //if there are more documents then send them as attachments
    if (documents.length > 1) {
        const response = [receipt];
        const documentProtocolNo = receipt.DocumentProtocolNo;
        for (let i = 1; i < documents.length; i++) {
            const document = documents[i];
            const AttachmentRequest = {
                FileName: '' +
                    (document.FileName ||
                        (typeof document.FileDataOrPath === 'string'
                            ? document.FileDataOrPath
                                .split('/')
                                .pop()
                            : 'attachment' + i + '.pdf')),
                Comments: document.Comments || '',
                IsFinal: i === documents.length - 1,
            };
            if (!AttachmentRequest.FileName)
                throw new Error('FileName is required');
            const attachmentData = (0, fileToBuffer_1.fileToBuffer)(document.FileDataOrPath);
            const attachmentResp = yield sendDocument(token, attachmentData, AttachmentRequest, true, documentProtocolNo);
            response.push(attachmentResp);
        }
        return response;
    }
    return [receipt];
});
exports.posToShde = posToShde;
exports.default = exports.posToShde;
