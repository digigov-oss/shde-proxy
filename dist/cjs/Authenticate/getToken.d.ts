export declare const getToken: (config?: {
    apiBase?: string;
    sectorCode?: string;
    clientId?: string;
    clientSecret?: string;
}) => Promise<any>;
export default getToken;
