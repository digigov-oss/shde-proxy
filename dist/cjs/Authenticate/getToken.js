"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getToken = void 0;
const axios_1 = __importDefault(require("axios"));
const authenticate = (apiBase, sectorCode, clientId, clientSecret) => __awaiter(void 0, void 0, void 0, function* () {
    const response = yield axios_1.default.post(`${apiBase}/authenticate`, {
        SectorCode: parseInt(sectorCode),
        ClientId: clientId,
        ClientSecret: clientSecret,
    });
    return response.data;
});
const getToken = (config) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d;
    const apiBase = (_a = config === null || config === void 0 ? void 0 : config.apiBase) !== null && _a !== void 0 ? _a : process.env.KSHDE_API_BASE;
    if (!apiBase) {
        throw new Error('Missing KSHDE api base url');
    }
    const sectorCode = (_b = config === null || config === void 0 ? void 0 : config.sectorCode) !== null && _b !== void 0 ? _b : process.env.KSHDE_SECTOR_CODE;
    if (!sectorCode) {
        throw new Error('Missing KSHDE sector code');
    }
    const clientId = (_c = config === null || config === void 0 ? void 0 : config.clientId) !== null && _c !== void 0 ? _c : process.env.KSHDE_CLIENT_ID;
    if (!clientId) {
        throw new Error('Missing KSHDE client id');
    }
    const clientSecret = (_d = config === null || config === void 0 ? void 0 : config.clientSecret) !== null && _d !== void 0 ? _d : process.env.KSHDE_CLIENT_SECRET;
    if (!clientSecret) {
        throw new Error('Missing KSHDE client secret');
    }
    const response = yield authenticate(apiBase, sectorCode, clientId, clientSecret);
    return response.AccessToken;
});
exports.getToken = getToken;
exports.default = exports.getToken;
