import config from './config.json';
if (!process.env.KSHDE_API_BASE) {
    console.info('KSHDE_API_BASE is not set, trying to get it from config.json');
    if (process.env.NODE_ENV === 'development')
        process.env.KSHDE_API_BASE = config.test;
    else
        process.env.KSHDE_API_BASE = config.prod;
}
//export functions
export { getOrgChart } from './OrgChart';
export { getOrganizations } from './OrgChart/getOrganizations';
export { getToken } from './Authenticate/getToken';
export { posToShde } from './SubmitDocument';
//export classes
export { default as OrgChartDB } from './OrgChart/db/orgchart';
//export utils
export { fileToBuffer } from './SubmitDocument/utils/fileToBuffer';
