import type { Response, Document } from './types';
export declare const posToShde: (documents: Document[], recipientCode: number | number[], orgChartVersion?: number, config?: {
    apiBase?: string;
    sectorCode?: string;
    clientId?: string;
    clientSecret?: string;
}) => Promise<Response[]>;
export default posToShde;
