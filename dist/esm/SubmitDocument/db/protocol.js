//if not protocol provided then will use our generator,
import Client from 'pg-native';
const protocolSEQ = (connectionString = '', pnreset) => {
    const POSTGRES_USER = process.env.PGUSER;
    const POSTGRES_PASSWORD = process.env.PGPASSWORD;
    const POSTGRES_DB = process.env.PGDATABASE;
    const POSTGRES_HOST = process.env.PGHOST;
    const POSTGRES_PORT = process.env.PGPORT;
    if (connectionString === '')
        connectionString = `postgres://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_DB}`;
    const client = new Client();
    client.connectSync(connectionString);
    try {
        let protocol_split = 'aion';
        let protocol_date = new Date().toISOString().split('T')[0];
        switch (pnreset) {
            case 'daily':
                protocol_split = protocol_date;
                break;
            case 'monthly':
                protocol_split =
                    protocol_date.split('-')[0] +
                        '-' +
                        protocol_date.split('-')[1];
                break;
            case 'yearly':
                protocol_split = protocol_date.split('-')[0];
                break;
            case 'innumerable':
                protocol_split = 'aion';
                break;
        }
        const seqName = 'prot' + protocol_split.replace(/-/g, '');
        //create sequence for protocol_split if not exists
        client.querySync('CREATE SEQUENCE IF NOT EXISTS ' + seqName + '_seq START 1');
        const res = client.querySync("SELECT nextval('" + seqName + "_seq');");
        return res[0].nextval + '/' + protocol_date;
    }
    catch (error) {
        throw error;
    }
};
export default protocolSEQ;
