export const validateDocuments = (documents) => {
    const fileNames = [];
    documents.forEach((document) => {
        if (!document.FileDataOrPath)
            throw new Error('FileDataOrPath is required');
        if (document.FileName) {
            if (fileNames.includes(document.FileName))
                throw new Error(`FileName ${document.FileName} is not unique`);
            //filename must not contain special characters : ; / \ * % < > | "
            if (/[;/:*%<>|"]/.test(document.FileName))
                throw new Error(`FileName ${document.FileName} contains special characters`);
            fileNames.push(document.FileName);
        }
    });
};
