/// <reference types="node" />
export declare const fileToBuffer: (fileDataOrPath: Uint8Array | string) => Buffer;
export default fileToBuffer;
