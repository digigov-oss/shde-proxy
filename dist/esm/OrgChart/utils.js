export const flatten = (ChildNodes) => {
    return ChildNodes.reduce((acc, node) => {
        const { ChildNodes, ...rest } = node;
        return [
            ...acc,
            rest,
            ...flatten(ChildNodes).map((n) => ({
                ...n,
                ParentCode: rest.Code,
            })),
        ];
    }, []);
};
