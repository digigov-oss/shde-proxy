import { Organization } from './types';
export declare const getOrganizations: (token: string, config?: {
    apiBase?: string;
}) => Promise<Organization[]>;
export default getOrganizations;
