import { DatabaseSettings } from './types';
export declare function getOrgChart(dryRun?: boolean, dbSettings?: DatabaseSettings): Promise<any>;
export default getOrgChart;
