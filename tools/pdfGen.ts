/*generates a pdf file with a given text*/
import fs from 'fs';
const pdfTemplate = `
%PDF-1.2\n
9 0 obj\n<<\n>>\nstream\nBT/F1 14 Tf(___TEXTPLACEHOLDER___)' ET\nendstream\nendobj\n
4 0 obj\n<<\n/Type /Page\n/Parent 5 0 R\n/Contents 9 0 R\n>>\nendobj\n
5 0 obj\n<<\n/Kids [4 0 R ]\n/Count 1\n/Type /Pages\n/MediaBox [ 0 0 612 792 ]/Resources\n<</Font<</F1 2 0 R>>\n>>\n>>\nendobj\n
3 0 obj\n<<\n/Pages 5 0 R\n/Type /Catalog\n>>\nendobj\n
2 0 obj\n<<\n/Type /Font\n/Subtype /Type1\n/Name /F1\n/BaseFont/Helvetica\n>>\nendobj\n
trailer\n<<\n/Root 3 0 R\n>>\n
%%EOF
`;
const pdfGen = (text: string, path: string = '') => {
    const pdf = pdfTemplate.replace('___TEXTPLACEHOLDER___', text);
    if (path != '') fs.writeFileSync(path, pdf);
    return pdf;
};

export default pdfGen;
