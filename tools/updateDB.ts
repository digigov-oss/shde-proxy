//NODE_ENV=development node --experimental-specifier-resolution=node --experimental-modules --no-warnings --loader ts-node/esm ./updateDB.ts
// Purpose: Update the database with the latest org chart data.
import dotenv from "dotenv";
dotenv.config({path:'../.env'});
import { getOrgChart } from "../src";

const orgChart = await getOrgChart();
console.log(orgChart);